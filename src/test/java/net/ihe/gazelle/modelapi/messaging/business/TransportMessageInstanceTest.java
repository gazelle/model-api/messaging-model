package net.ihe.gazelle.modelapi.messaging.business;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TransportMessageInstanceTest {

    private static final String UUID = "uuid";
    private static final String STANDARD_KEYWORD = "keyword";

    @Test
    void constructor() {
        TransportMessageInstance transportMessageInstance = new TransportMessageInstance(UUID, STANDARD_KEYWORD) {
        };

        assertEquals(UUID, transportMessageInstance.getUuid(), "Constructor shall set UUID property with input value !");
        assertEquals(STANDARD_KEYWORD, transportMessageInstance.getStandardKeyword(), "Constructor shall set Standard Keyword property with" +
                " input value !");
    }
}
