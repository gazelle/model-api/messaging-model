package net.ihe.gazelle.modelapi.messaging.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link TransactionOperationInstance}.
 */
public class TransactionOperationInstanceTest {

    private static final String UUID = "uuid";
    private static final String UUID2 = "uuid2";
    private static final Date TIMESTAMP = new Date();
    private static ConnectionPoint INITIATOR;
    private static ConnectionPoint RESPONDER;

    /**
     * Prepare Initiator and Responder to use in tests.
     *
     * @throws UnknownHostException if ConnectionPoints cannot be instantiated.
     */
    @BeforeAll
    public static void prepare() throws UnknownHostException {
        String actor = "Actor";
        INITIATOR = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR,
                actor);
        RESPONDER = new ConnectionPoint(InetAddress.getByName("127.0.0.2"), PeerType.SIMULATOR,
                actor);
    }
    /**
     * Test for the UUID property getter
     */
    @Test
    @Covers(requirements = {"SIMU-060"})
    public void getUuidTest() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, new Date(), new ArrayList<>(), INITIATOR
                , RESPONDER);

        assertEquals(UUID, transactionOperationInstance.getUuid(), "Getter shall return the property value.");
    }

    /**
     * Test for the UUID property setter for a valid value (not null).
     */
    @Test
    @Covers(requirements = {"SIMU-060"})
    public void setUuidTest() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance("Tarte", new Date(), new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setUuid(UUID);

        assertEquals(UUID, transactionOperationInstance.getUuid(), "Setter shall update the value of the property.");
    }

    /**
     * Test for the UUID property setter for a not valid value (null).
     */
    @Test
    @Covers(requirements = {"SIMU-060"})
    public void setUuidTestNull() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, new Date(), new ArrayList<>(), INITIATOR, RESPONDER);

        assertThrows(IllegalArgumentException.class, () -> transactionOperationInstance.setUuid(null),
                "IllegalArgumentException shall be thrown as uuid shall not be null.");
    }

    /**
     * Test for the openingTimestamp property getter.
     */
    @Test
    @Covers(requirements = {"SIMU-061"})
    public void getOpeningTimestampTest() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        assertEquals(TIMESTAMP, transactionOperationInstance.getOpeningTimestamp(), "Getter shall return the value of the Timestamp.");
    }

    /**
     * Test for the openingTimestamp property setter for a valid value (not null).
     */
    @Test
    @Covers(requirements = {"SIMU-061"})
    public void setOpeningTimestampTest() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, new Date(), new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setOpeningTimestamp(TIMESTAMP);

        assertEquals(TIMESTAMP, transactionOperationInstance.getOpeningTimestamp(), "Setter shall update the value of the timestamp property.");
    }

    /**
     * Test for the openingTimestamp property setter for a not valid value (null).
     */
    @Test
    @Covers(requirements = {"SIMU-061"})
    public void setOpeningTimestampTestNull() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        assertThrows(IllegalArgumentException.class, () -> transactionOperationInstance.setOpeningTimestamp(null),
                "IllegalArgumentException shall be thrown as openingTimestamp shall not be null.");
    }

    /**
     * Test for closingTimestamp property getter and setter for a non null value.
     */
    @Test
    @Covers(requirements = {"SIMU-061"})
    public void getSetClosingTimestampTest() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, new Date(), new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setClosingTimestamp(TIMESTAMP);

        assertEquals(TIMESTAMP, transactionOperationInstance.getClosingTimestamp(), "Setter shall update the value of the closingTimestamp.");
    }

    /**
     * Test for closingTimestamp property getter and setter for a null value.
     */
    @Test
    @Covers(requirements = {"SIMU-061"})
    public void getSetClosingTimestampTestNull() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, new Date(), new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setClosingTimestamp(TIMESTAMP);
        transactionOperationInstance.setClosingTimestamp(null);

        assertEquals(null, transactionOperationInstance.getClosingTimestamp(), "Closing timestamp shall be able to be null.");
    }

    /**
     * Test for standardKeywords property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-062"})
    public void getSetStandardKeywordsTest() {
        List<String> standardKeywords = new ArrayList<>();
        standardKeywords.add("standard");
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, standardKeywords, INITIATOR, RESPONDER);

        assertEquals(standardKeywords, transactionOperationInstance.getStandardKeywords(), "Getter shall return the value of standard keyword list.");
    }

    /**
     * Test for standardKeywords property add method.
     */
    @Test
    @Covers(requirements = {"SIMU-062"})
    public void addStandardKeywordTest() {
        String standardKeyword = "standard";
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.addStandardKeyword(standardKeyword);

        assertTrue(transactionOperationInstance.getStandardKeywords().contains(standardKeyword), "Standard keyword shall be added to the list !");
    }

    /**
     * Test for the getter and setter of the standardKeywords property with a null value.
     */
    @Test
    @Covers(requirements = {"SIMU-062"})
    public void setStandardKeywordsTest() {
        List<String> standardKeywords = new ArrayList<>();
        standardKeywords.add("standard");
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, standardKeywords, INITIATOR, RESPONDER);

        transactionOperationInstance.setStandardKeywords(null);

        assertNotNull(transactionOperationInstance.getStandardKeywords(), "Standard keyword shall only be emptied when set to null !");
        assertTrue(transactionOperationInstance.getStandardKeywords().isEmpty(), "Standard keyword shall be empty when set to null !");
    }

    /**
     * Test for domainKeyword property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-063"})
    public void getSetDomainKeywordTest() {
        String domainKeyword = "domain";
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setDomainKeyword(domainKeyword);

        assertEquals(domainKeyword, transactionOperationInstance.getDomainKeyword(), "Getter did not return the set value for DomainKeyword !");
    }

    /**
     * Test for the transactionKeyword property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-064"})
    public void getSetTransactionKeywordTest() {
        String transactionKeyword = "transaction";
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setTransactionKeyword(transactionKeyword);

        assertEquals(transactionKeyword, transactionOperationInstance.getTransactionKeyword(), "Getter did not return the set value for " +
                "TransactionKeyword !");
    }

    /**
     * Test for the operationKeyword property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-065"})
    public void getSetOperationKeywordTest() {
        String operationKeyword = "operation";
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setOperationKeyword(operationKeyword);

        assertEquals(operationKeyword, transactionOperationInstance.getOperationKeyword(), "Getter did not return the set value for OperationKeyword !");
    }

    /**
     * Test for the initiator property getter and setter.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    @Covers(requirements = {"SIMU-066"})
    public void getSetInitiatorTest() throws UnknownHostException {
        ConnectionPoint initiator = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor");
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setInitiator(initiator);

        assertEquals(initiator, transactionOperationInstance.getInitiator(), "Getter did not return the set value for Initiator !");
    }

    /**
     * Test for the initiator property setter with a null value.
     */
    @Test
    @Covers(requirements = {"SIMU-075"})
    public void setInitiatorTestNull() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);

        assertThrows(IllegalArgumentException.class, () -> transactionOperationInstance.setInitiator(null),
                "IllegalArgumentException shall be thrown as initiator shall not be null.");
    }

    /**
     * Test for the responder property getter and setter.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    @Covers(requirements = {"SIMU-066"})
    public void getSetResponderTest() throws UnknownHostException {
        ConnectionPoint responder = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor");
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setResponder(responder);

        assertEquals(responder, transactionOperationInstance.getResponder(), "Getter did not return the set value for Responder !");
    }

    /**
     * Test for the responder property setter with a null value.
     */
    @Test
    @Covers(requirements = {"SIMU-075"})
    public void setResponderTestNull() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);

        assertThrows(IllegalArgumentException.class, () -> transactionOperationInstance.setResponder(null),
                "IllegalArgumentException shall be thrown as uuid shall not be null.");
    }

    /**
     * Test for abstractMessageInstances property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-067"})
    public void getSetAbstractMessageInstancesTest() {
        List<MessageInstance> messageInstances = new ArrayList<>();
        MessageInstance messageInstance = new MessageInstance("Test", INITIATOR, RESPONDER);
        messageInstances.add(messageInstance);
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setMessageInstances(messageInstances);

        assertEquals(messageInstances, transactionOperationInstance.getMessageInstances(), "Getter did not return the set value for MessageInstances !");
    }

    /**
     * Test for abstractMessageInstances property add method.
     */
    @Test
    @Covers(requirements = {"SIMU-067"})
    public void addAbstractMessageInstanceTest() {
        MessageInstance messageInstance = new MessageInstance("Test", INITIATOR, RESPONDER);
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.addAbstractMessageInstance(messageInstance);

        assertTrue(transactionOperationInstance.getMessageInstances().contains(messageInstance), "MessageInstance has not been correctly added !");
    }

    /**
     * Test for the getter and setter of the abstractMessageInstances property with a null value.
     */
    @Test
    @Covers(requirements = {"SIMU-067"})
    public void setAbstractMessageInstancesTest() {
        List<MessageInstance> messageInstances = new ArrayList<>();
        MessageInstance messageInstance = new MessageInstance("Test", INITIATOR, RESPONDER);
        messageInstances.add(messageInstance);
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        transactionOperationInstance.setMessageInstances(messageInstances);
        transactionOperationInstance.setMessageInstances(null);

        assertNotNull(transactionOperationInstance.getMessageInstances(), "MessageInstances shall only be emptied when set to null.");
        assertTrue(transactionOperationInstance.getMessageInstances().isEmpty(), "MessageInstance shall be empty when set to null.");
    }

    /**
     * Test for the equals method with a null object.
     */
    @Test
    public void equalsNull() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);

        assertFalse(transactionOperationInstance.equals(null), "TransactionOperationInstance shall not be equal to null.");
    }

    /**
     * Test for the equals method with an instance of another class.
     */
    @Test
    public void equalsOtherClass() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);

        assertFalse(transactionOperationInstance.equals(3), "TransactionOperationInstance shall not be equal to an instance of another class.");
    }

    /**
     * Test for the equals method with twice the same object.
     */
    @Test
    public void equalsItself() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);

        assertTrue(transactionOperationInstance.equals(transactionOperationInstance), "TransactionOperationInstance shall be equal to itself.");
    }

    /**
     * Test for the identityEquals method with two objects having different UUID.
     */
    @Test
    public void identityEqualsTest() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID2, TIMESTAMP, new ArrayList<>(),
                INITIATOR, RESPONDER);

        assertFalse(transactionOperationInstance.identityEquals(transactionOperationInstance2), "TransactionOperationInstances with different" +
                "UUID shall not have the same identity !");
    }

    /**
     * Test for the equals method with two objects having different openingTimestamp.
     */
    @Test
    public void equalsTestDifferentOpeningTimestamp() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, new Date(), new ArrayList<>(),
                INITIATOR, RESPONDER);

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different OpeningTimestamp" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different closingTimestamp.
     */
    @Test
    public void equalsTestDifferentClosingTimestamp() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        transactionOperationInstance.setClosingTimestamp(TIMESTAMP);
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);
        transactionOperationInstance2.setClosingTimestamp(new Date());

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different ClosingTimestamp" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different closingTimestamp, one having none (null).
     */
    @Test
    public void equalsTestDifferentClosingTimestampNull() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        transactionOperationInstance.setClosingTimestamp(TIMESTAMP);
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR
                , RESPONDER);

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different " +
                "ClosingTimestamp" +
                "shall not be equal.");
        assertFalse(transactionOperationInstance2.equals(transactionOperationInstance), "TransactionOperationInstances with different ClosingTimestamp" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different list of standard keywords.
     */
    @Test
    public void equalsTestDifferentStandardKeywords(){
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        transactionOperationInstance.addStandardKeyword("standard");
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR
                , RESPONDER);

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different StandardKeywords" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different domainKeyword.
     */
    @Test
    public void equalsTestDifferentDomainKeyword() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        transactionOperationInstance.setDomainKeyword("domain");
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR
                , RESPONDER);
        transactionOperationInstance2.setDomainKeyword("domain2");

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different DomainKeyword" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different domainKeyword, one having none (null).
     */
    @Test
    public void equalsTestDifferentDomainKeywordNull() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        transactionOperationInstance.setDomainKeyword("domain");
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR
                , RESPONDER);

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different DomainKeyword" +
                "shall not be equal.");
        assertFalse(transactionOperationInstance2.equals(transactionOperationInstance), "TransactionOperationInstances with different DomainKeyword" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different transactionKeyword.
     */
    @Test
    public void equalsTestDifferentTransactionKeyword() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        transactionOperationInstance.setTransactionKeyword("transaction");
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR
                , RESPONDER);
        transactionOperationInstance2.setTransactionKeyword("transaction2");

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different TransactionKeyword" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different transactionKeyword, one having none (null).
     */
    @Test
    public void equalsTestDifferentTransactionKeywordNull() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        transactionOperationInstance.setTransactionKeyword("transaction");
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR
                , RESPONDER);

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different " +
                "TransactionKeyword" +
                "shall not be equal.");
        assertFalse(transactionOperationInstance2.equals(transactionOperationInstance), "TransactionOperationInstances with different TransactionKeyword" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different operationKeyword.
     */
    @Test
    public void equalsTestDifferentOperationKeyword() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        transactionOperationInstance.setOperationKeyword("operation");
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR
                , RESPONDER);
        transactionOperationInstance2.setOperationKeyword("operation2");

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different OperationKeyword" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different operationKeyword, one having none (null).
     */
    @Test
    public void equalsTestDifferentOperationKeywordNull() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        transactionOperationInstance.setOperationKeyword("operation");
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR
                , RESPONDER);

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different OperationKeyword" +
                "shall not be equal.");
        assertFalse(transactionOperationInstance2.equals(transactionOperationInstance), "TransactionOperationInstances with different OperationKeyword" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different initiator.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    public void equalsTestDifferentInitiator() throws UnknownHostException {
        ConnectionPoint initiator = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor");
        ConnectionPoint initiator2 = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor2");
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);
        transactionOperationInstance.setInitiator(initiator);
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);
        transactionOperationInstance2.setInitiator(initiator2);

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different Initiator" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different responder.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    public void equalsTestDifferentResponder() throws UnknownHostException {
        ConnectionPoint responder = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor");
        ConnectionPoint responder2 = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor2");
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                responder);
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, responder2);

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different Responder" +
                "shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different responder, one having none (null).
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    public void equalsTestDifferentResponderNull() throws UnknownHostException {
        ConnectionPoint responder = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor");
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR,
                RESPONDER);
        transactionOperationInstance.setResponder(responder);
        TransactionOperationInstance transactionOperationInstance2 = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR
                , RESPONDER);

        assertFalse(transactionOperationInstance.equals(transactionOperationInstance2), "TransactionOperationInstances with different Responder" +
                "shall not be equal.");
        assertFalse(transactionOperationInstance2.equals(transactionOperationInstance), "TransactionOperationInstances with different Responder" +
                "shall not be equal.");
    }

    /**
     * Test for the hashCode method.
     */
    @Test
    public void hashCodeTest() {
        TransactionOperationInstance transactionOperationInstance = new TransactionOperationInstance(UUID, TIMESTAMP, new ArrayList<>(), INITIATOR, RESPONDER);

        assertNotNull(transactionOperationInstance.hashCode(), "Hash code shall not be null.");
    }
}
