package net.ihe.gazelle.modelapi.messaging.business;

public class MessagePartInstance extends AbstractMessagePartInstance {

    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid            value of the entity UUID
     * @param standardKeyword keyword of the standard
     */
    public MessagePartInstance(String uuid, String standardKeyword) {
        super(uuid, standardKeyword);
    }
}
