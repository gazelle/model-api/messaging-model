package net.ihe.gazelle.modelapi.messaging.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link MessageMetadataInstance}
 */
public class MessageMetadataInstanceTest {

    private static final String NAME = "Name";
    private static final String NAME2 = "Name2";
    private static final String VALUE = "Value";
    private static final String VALUE2 = "Value2";

    /**
     * Test for the Name property Getter.
     */
    @Test
    @Covers(requirements = {"SIMU-078"})
    public void getNameTest() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);

        assertEquals(NAME, messageMetadataInstance.getName(), "The name for the MessageMetadataInstance returned should be the one set.");
    }

    /**
     * Test for the Name property Setter for a not null value.
     */
    @Test
    @Covers(requirements = {"SIMU-078"})
    public void setNameTest() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);

        messageMetadataInstance.setName(NAME2);

        assertEquals(NAME2, messageMetadataInstance.getName(), "The name for the MessageMetadataInstance returned should be the one set.");
    }

    /**
     * Test for the Name property Setter with a null value.
     */
    @Test
    @Covers(requirements = {"SIMU-078"})
    public void setNameTestNull() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);

        assertThrows(IllegalArgumentException.class, () -> messageMetadataInstance.setName(null),
                "IllegalArgumentException shall be thrown as name shall not be null.");
    }

    /**
     * Test for the Value property Getter.
     */
    @Test
    @Covers(requirements = {"SIMU-078"})
    public void getValueTest() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);

        assertEquals(VALUE, messageMetadataInstance.getValue(), "The value for the MessageMetadataInstance returned should be the one set.");
    }

    /**
     * Test for the Value property Setter.
     */
    @Test
    @Covers(requirements = {"SIMU-078"})
    public void setValueTest() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);

        messageMetadataInstance.setValue(VALUE2);

        assertEquals(VALUE2, messageMetadataInstance.getValue(), "The value for the MessageMetadataInstance returned should be the one set.");
    }

    /**
     * Test for the Value property Setter with a null value.
     */
    @Test
    @Covers(requirements = {"SIMU-078"})
    public void setValueTestNull() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);

        assertThrows(IllegalArgumentException.class, () -> messageMetadataInstance.setValue(null),
                "IllegalArgumentException shall be thrown as value shall not be null.");
    }

    /**
     * Test for the equals method with a null object.
     */
    @Test
    public void equalsNull() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);

        assertFalse(messageMetadataInstance.equals(null), "MessageMetadataInstance shall not be equal to null.");
    }

    /**
     * Test for the equals method with an instance of another class.
     */
    @Test
    public void equalsOtherClass() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);

        assertFalse(messageMetadataInstance.equals(3), "MessageMetadataInstance shall not be equal to an instance of another class..");
    }

    /**
     * Test for the equals method with twice the same object.
     */
    @Test
    @Covers(requirements = {"SIMU-078"})
    public void equalsItself() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);

        assertTrue(messageMetadataInstance.equals(messageMetadataInstance), "A MessageMetadataInstance shall be equal to itself.");
    }

    /**
     * Test for the equals method with two objects having different names.
     */
    @Test
    public void equalsTestDifferentNames() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);
        MessageMetadataInstance messageMetadataInstance2 = new MessageMetadataInstance(NAME2, VALUE);

        assertFalse(messageMetadataInstance.equals(messageMetadataInstance2), "MessageMetadataInstances with different names shall not be equal.");
    }


    /**
     * Test for the equals method with two object having different values.
     */
    @Test
    public void equalsTestDifferentValue() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);
        MessageMetadataInstance messageMetadataInstance2 = new MessageMetadataInstance(NAME, VALUE2);

        assertFalse(messageMetadataInstance.equals(messageMetadataInstance2), "MessageMetadataInstances with different values shall not be equal.");
    }

    /**
     * Test for the hashCode method.
     */
    @Test
    public void hashCodeTest() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance(NAME, VALUE);

        assertNotNull(messageMetadataInstance.hashCode(), "Hash code shall not be null.");
    }
}