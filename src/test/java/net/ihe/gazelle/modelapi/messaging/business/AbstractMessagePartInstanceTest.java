package net.ihe.gazelle.modelapi.messaging.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.jupiter.api.Assertions.*;

class AbstractMessagePartInstanceTest {

    private static final String UUID = "uuid";
    private static final String UUID2 = "uuid2";
    private static final String STANDARD_KEYWORD = "keyword";
    private static final String STANDARD_KEYWORD2 = "keyword2";
    private static final String MESSAGE_PART_KEYWORD = "keyword";

    @Test
    void constructor() {
        AbstractMessagePartInstance abstractMessagePartInstance = new AbstractMessagePartInstance(UUID, STANDARD_KEYWORD){};

        assertEquals(UUID, abstractMessagePartInstance.getUuid(), "Constructor shall set the input UUID value in the corresponding property.");
        assertEquals(STANDARD_KEYWORD, abstractMessagePartInstance.getStandardKeyword(), "Constructor shall set the input standard keyword" +
                " value in the corresponding property.");
    }

    @Test
    @Covers(requirements = {"SIMU-083"})
    void getSetUuid() {
        AbstractMessagePartInstance abstractMessagePartInstance = new AbstractMessagePartInstance(UUID, STANDARD_KEYWORD){};

        abstractMessagePartInstance.setUuid(UUID2);

        assertEquals(UUID2, abstractMessagePartInstance.getUuid(), "Get UUID value shall be equal to the value set.");
    }

    @Test
    @Covers(requirements = {"SIMU-083"})
    void setUuidNull() {
        AbstractMessagePartInstance abstractMessagePartInstance = new AbstractMessagePartInstance(UUID, STANDARD_KEYWORD){};

        assertThrows(IllegalArgumentException.class, () -> abstractMessagePartInstance.setUuid(null), "UUID shall not be allowed to be null.");
    }

    @Test
    @Covers(requirements = {"SIMU-084"})
    void getSetStandardKeyword() {
        AbstractMessagePartInstance abstractMessagePartInstance = new AbstractMessagePartInstance(UUID, STANDARD_KEYWORD){};

        abstractMessagePartInstance.setStandardKeyword(STANDARD_KEYWORD2);

        assertEquals(STANDARD_KEYWORD2, abstractMessagePartInstance.getStandardKeyword(), "Get Standard Keyword value shall be equal to the set value.");
    }

    @Test
    @Covers(requirements = {"SIMU-085"})
    void getSetMessageInstance() throws UnknownHostException {
        ConnectionPoint sender = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "actor");
        ConnectionPoint receiver = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "actor");
        MessageInstance messageInstance = new MessageInstance(UUID, sender, receiver);
        AbstractMessagePartInstance abstractMessagePartInstance = new AbstractMessagePartInstance(UUID2, STANDARD_KEYWORD){};

        abstractMessagePartInstance.setMessageInstance(messageInstance);

        assertEquals(messageInstance, abstractMessagePartInstance.getMessageInstance(), "Get MessageInstance shall be equal to set Message Instance" +
                ".");
        assertTrue(messageInstance.getAbstractMessagePartInstances().contains(abstractMessagePartInstance), "When a MessageInstance is set to " +
                "an AbstractMessageInstancePart, the MessageInstance shall also be linked to the Message Part.");

        abstractMessagePartInstance.setMessageInstance(null);

        assertNull(abstractMessagePartInstance.getMessageInstance(), "Get MessageInstance shall be equal to set Message Instance.");
        assertFalse(messageInstance.getAbstractMessagePartInstances().contains(abstractMessagePartInstance), "When a MessageInstance is unliked " +
                "from " +
                "an AbstractMessageInstancePart, the MessageInstance shall also be unlinked from the Message Part.");
    }

    @Test
    @Covers(requirements = {"SIMU-86"})
    void getSetMessagePartKeyword() {
        AbstractMessagePartInstance abstractMessagePartInstance = new AbstractMessagePartInstance(UUID, STANDARD_KEYWORD){};

        abstractMessagePartInstance.setMessagePartKeyword(MESSAGE_PART_KEYWORD);

        assertEquals(MESSAGE_PART_KEYWORD, abstractMessagePartInstance.getMessagePartKeyword(), "Get message part keyword shall be the same as " +
                "set message part keyword.");
    }

    /**
     * Test for the identityEquals method with two objects having different UUID.
     */
    @Test
    public void identityEqualsTestDifferentUUID() {
        MessagePartInstance abstractMessagePartInstance = new MessagePartInstance(UUID, STANDARD_KEYWORD);
        MessagePartInstance abstractMessagePartInstance2 = new MessagePartInstance(UUID2, STANDARD_KEYWORD){};

        assertFalse(abstractMessagePartInstance.identityEquals(abstractMessagePartInstance2), "MessagePartInstance with different UUID shall not " +
                "have the same identity.");
    }

    /**
     * Test for the identityEquals method with object not an AbstractMessagePart.
     */
    @Test
    public void identityEqualsTestDifferentObject() {
        MessagePartInstance abstractMessagePartInstance = new MessagePartInstance(UUID, STANDARD_KEYWORD);

        assertFalse(abstractMessagePartInstance.identityEquals(new Object()), "foreign object shall not be equal");
    }

    /**
     * Test for the identityEquals method with two objects having same UUID.
     */
    @Test
    public void identityEqualsTestSameUUID() {
        MessagePartInstance abstractMessagePartInstance = new MessagePartInstance(UUID, STANDARD_KEYWORD);
        MessagePartInstance abstractMessagePartInstance2 = new MessagePartInstance(UUID, STANDARD_KEYWORD){};

        assertTrue(abstractMessagePartInstance.identityEquals(abstractMessagePartInstance2), "MessagePartInstance with same UUID shall " +
                "have the same identity.");
    }

    /**
     * Test for the identityEquals method withsame object.
     */
    @Test
    public void identityEqualsTestSameObject() {
        MessagePartInstance abstractMessagePartInstance = new MessagePartInstance(UUID, STANDARD_KEYWORD);

        assertTrue(abstractMessagePartInstance.identityEquals(abstractMessagePartInstance), "object shall be equal to itself");
    }

    /**
     * Test for the equals method with twice the same object.
     */
    @Test
    public void equalsItself() {
        MessagePartInstance abstractMessagePartInstance = new MessagePartInstance(UUID, STANDARD_KEYWORD);

        assertTrue(abstractMessagePartInstance.equals(abstractMessagePartInstance), "A MessagePartInstance shall be equal to itself.");
    }

    /**
     * Test for the equal method with two objects having different standard keyword.
     */
    @Test
    public void equalsTestDifferentStandardKeyword() {
        MessagePartInstance abstractMessagePartInstance = new MessagePartInstance(UUID, STANDARD_KEYWORD);
        MessagePartInstance abstractMessagePartInstance2 = new MessagePartInstance(UUID, STANDARD_KEYWORD2);

        assertFalse(abstractMessagePartInstance.equals(abstractMessagePartInstance2), "MessagePartInstances with different standard keyword" +
                " shall not be equal.");
    }

    /**
     * Test for the equal method with two objects having different UUID.
     */
    @Test
    public void equalsTestDifferentUUID() {
        MessagePartInstance abstractMessagePartInstance = new MessagePartInstance(UUID, STANDARD_KEYWORD);
        MessagePartInstance abstractMessagePartInstance2 = new MessagePartInstance(UUID2, STANDARD_KEYWORD);

        assertTrue(abstractMessagePartInstance.equals(abstractMessagePartInstance2), "MessagePartInstances with different UUID" +
                " may be equal.");
    }

    /**
     * Test for the equals method with two objects having different Message Keyword.
     */
    @Test
    public void equalsTestDifferentMessagePartKeyword() {
        MessagePartInstance abstractMessagePartInstance = new MessagePartInstance(UUID, STANDARD_KEYWORD){};
        abstractMessagePartInstance.setMessagePartKeyword("Tarte");
        MessagePartInstance abstractMessagePartInstance2 = new MessagePartInstance(UUID, STANDARD_KEYWORD){};
        abstractMessagePartInstance2.setMessagePartKeyword("Pommes");

        assertFalse(abstractMessagePartInstance.equals(abstractMessagePartInstance2), "MessagePartInstance with different message part keyword" +
                " shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having same Message Keyword.
     */
    @Test
    public void equalsTestSameMessagePartKeyword() {
        MessagePartInstance abstractMessagePartInstance = new MessagePartInstance(UUID, STANDARD_KEYWORD){};
        abstractMessagePartInstance.setMessagePartKeyword("Tarte");
        MessagePartInstance abstractMessagePartInstance2 = new MessagePartInstance(UUID, STANDARD_KEYWORD){};
        abstractMessagePartInstance2.setMessagePartKeyword("Tarte");

        assertTrue(abstractMessagePartInstance.equals(abstractMessagePartInstance2), "MessagePartInstance with same message part keyword" +
                " shall be equal.");
    }

    /**
     * Test for the equals method with two objects having different Message Instance.
     */
    @Test
    public void equalsTestDifferentMessageInstance() throws UnknownHostException {
        ConnectionPoint sender = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "actor");
        ConnectionPoint sender2 = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "actor3");
        ConnectionPoint receiver = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SYSTEM_UNDER_TEST, "actor2");
        MessagePartInstance abstractMessagePartInstance = new MessagePartInstance(UUID, STANDARD_KEYWORD){};
        abstractMessagePartInstance.setMessageInstance(new MessageInstance(UUID, sender, receiver));
        MessagePartInstance abstractMessagePartInstance2 = new MessagePartInstance(UUID, STANDARD_KEYWORD){};
        abstractMessagePartInstance2.setMessageInstance(new MessageInstance(UUID2, sender2, receiver));

        assertFalse(abstractMessagePartInstance.equals(abstractMessagePartInstance2), "MessagePartInstance with different MessageInstance" +
                " shall not be equal.");
    }

    /**
     * Test for the hashCode method.
     */
    @Test
    public void hashCodeTest() {
        AbstractMessagePartInstance abstractMessagePartInstance = new AbstractMessagePartInstance(UUID, STANDARD_KEYWORD){};

        assertNotNull(abstractMessagePartInstance.hashCode(), "Hash code shall not be null.");
    }

}
