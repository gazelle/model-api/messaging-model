package net.ihe.gazelle.modelapi.messaging.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link MessageInstance}.
 */
public class MessageInstanceTest {

    private static final String UUID = "uuid";
    private static final String UUID2 = "uuid2";
    private static final String MESSAGE_KEYWORD = "MessageKeyword";
    private static final Date TIMESTAMP = new Date();
    private static ConnectionPoint SENDER;
    private static ConnectionPoint RECEIVER;

    /**
     * Prepare Sender and Receiver to use in tests.
     *
     * @throws UnknownHostException if ConnectionPoints cannot be instantiated.
     */
    @BeforeAll
    public static void prepare() throws UnknownHostException {
        String actor = "Actor";
        SENDER = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR,
                actor);
        RECEIVER = new ConnectionPoint(InetAddress.getByName("127.0.0.2"), PeerType.SIMULATOR,
                actor);
    }

    /**
     * Test for the UUID property getter.
     */
    @Test
    @Covers(requirements = {"SIMU-072"})
    public void getUuidTest() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        assertEquals(UUID, messageInstance.getUuid(), "Constructor shall set the UUID value to the property.");
    }

    /**
     * Test for the UUID property setter.
     */
    @Test
    @Covers(requirements = {"SIMU-072"})
    public void setUuidTest() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        messageInstance.setUuid(UUID2);

        assertEquals(UUID2, messageInstance.getUuid(), "Get UUID value shall be equals to the set value.");
    }

    /**
     * Test for the UUID property setter with a null value.
     */
    @Test
    @Covers(requirements = {"SIMU-072"})
    public void setUuidTestNull() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        assertThrows(IllegalArgumentException.class, () -> messageInstance.setUuid(null),
                "IllegalArgumentException shall be thrown as uuid shall not be null.");
    }

    /**
     * Test for the messageKeyword property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-073"})
    public void getSetMessageKeywordTest() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        messageInstance.setMessageKeyword(MESSAGE_KEYWORD);

        assertEquals(MESSAGE_KEYWORD, messageInstance.getMessageKeyword(), "Get MessageKeyword shall be equal to set MessageKeyword.");
    }

    /**
     * Test for the timestamp property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-074"})
    public void getSetTimestampTest() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        messageInstance.setTimestamp(TIMESTAMP);

        assertEquals(TIMESTAMP, messageInstance.getTimestamp(), "Get Timestamp shall be equal to set Timestamp.");
    }

    /**
     * Test for the sender property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-075"})
    public void getSetSenderTest() throws UnknownHostException {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        ConnectionPoint sender = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor");

        messageInstance.setSender(sender);

        assertEquals(sender, messageInstance.getSender(), "Get Sender shall be equal to set Sender.");
    }

    /**
     * Test for the sender property setter with a null value.
     */
    @Test
    @Covers(requirements = {"SIMU-075"})
    public void setSenderTestNull() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        assertThrows(IllegalArgumentException.class, () -> messageInstance.setSender(null),
                "IllegalArgumentException shall be thrown as uuid shall not be null.");
    }


    /**
     * Setter for the receiver property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-075"})
    public void getSetReceiverTest() throws UnknownHostException {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        ConnectionPoint receiver = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor");

        messageInstance.setReceiver(receiver);

        assertEquals(receiver, messageInstance.getReceiver(), "Get Receiver shall be equal to set Receiver.");
    }

    /**
     * Test for the receiver property setter with a null value.
     */
    @Test
    @Covers(requirements = {"SIMU-075"})
    public void setReceiverTestNull() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        assertThrows(IllegalArgumentException.class, () -> messageInstance.setReceiver(null),
                "IllegalArgumentException shall be thrown as uuid shall not be null.");
    }

    /**
     * Setter for the messageMetadataInstances property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-076"})
    public void getSetMessageMetadataInstancesTest() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        List<MessageMetadataInstance> metadataInstances = new ArrayList<>();
        MessageMetadataInstance metadataInstance = new MessageMetadataInstance("Test", "Test");
        metadataInstances.add(metadataInstance);

        messageInstance.setMessageMetadataInstances(metadataInstances);

        assertEquals(metadataInstances, messageInstance.getMessageMetadataInstances(), "Get MessageMetadataInstances shall be the same as set value" +
                ".");
        metadataInstances.remove(metadataInstance);
        assertEquals(1, messageInstance.getMessageMetadataInstances().size(), "Removed MetadataInstance shall reduce list size.");
    }

    /**
     * Setter for the messageMetadataInstances property add method.
     */
    @Test
    @Covers(requirements = {"SIMU-076"})
    public void addMessageMetadataInstanceTest() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        List<MessageMetadataInstance> metadataInstances = new ArrayList<>();
        MessageMetadataInstance metadataInstance = new MessageMetadataInstance("Test", "Test");
        metadataInstances.add(metadataInstance);
        messageInstance.setMessageMetadataInstances(metadataInstances);
        MessageMetadataInstance metadataInstance1 = new MessageMetadataInstance("Tarte", "Pommes");

        messageInstance.addMessageMetadataInstance(metadataInstance1);

        assertEquals(2, messageInstance.getMessageMetadataInstances().size(), "MessageMetadataInstances is not correctly set.");
        assertEquals(metadataInstance1, messageInstance.getMessageMetadataInstances().get(1),
                "MessageMetadataInstances shall have the same values as when set.");
    }

    /**
     * Test for the abstractMessagePartInstances property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-082"})
    public void getSetAbstractMessagePartInstancesTest() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        List<AbstractMessagePartInstance> messagePartInstances = new ArrayList<>();
        AbstractMessagePartInstance abstractMessagePartInstance = new AbstractMessagePartInstance("uuid", "keyword") {};

        messagePartInstances.add(abstractMessagePartInstance);
        messageInstance.setAbstractMessagePartInstances(messagePartInstances);
        assertEquals(messagePartInstances, messageInstance.getAbstractMessagePartInstances(), "MessagePartInstances is not correctly set !");
        assertEquals(messageInstance, abstractMessagePartInstance.getMessageInstance(), "MessageInstance shall also be set in MessagePartInstances.");
    }

    /**
     * Test for the abstractMessagePartInstances property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-082"})
    public void addAbstractMessagePartInstanceTest() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        List<AbstractMessagePartInstance> messagePartInstances = new ArrayList<>();
        AbstractMessagePartInstance abstractMessagePartInstance = new AbstractMessagePartInstance("uuid", "keyword") {};

        messagePartInstances.add(abstractMessagePartInstance);
        messageInstance.setAbstractMessagePartInstances(messagePartInstances);
        AbstractMessagePartInstance abstractMessagePartInstance2 = new AbstractMessagePartInstance("uuid2", "keyword2") {};
        messageInstance.addMessagePartInstance(abstractMessagePartInstance2);
        assertEquals(2, messageInstance.getAbstractMessagePartInstances().size(), "MessagePartInstance shall be added to the list.");
        assertEquals(messageInstance, abstractMessagePartInstance2.getMessageInstance(), "MessageInstance shall also be set to the part as added in the list.");
    }

    /**
     * Test for the abstractMessagePartInstances property getter and setter.
     */
    @Test
    @Covers(requirements = {"SIMU-082"})
    public void removeAbstractMessagePartInstanceTest() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        List<AbstractMessagePartInstance> messagePartInstances = new ArrayList<>();
        AbstractMessagePartInstance abstractMessagePartInstance = new AbstractMessagePartInstance("uuid", "keyword") {};
        messagePartInstances.add(abstractMessagePartInstance);
        messageInstance.setAbstractMessagePartInstances(messagePartInstances);

        messageInstance.removeMessagePartInstance(abstractMessagePartInstance);
        assertEquals(0, messageInstance.getAbstractMessagePartInstances().size(), "MessagePartInstance shall be removed from the list.");
        assertNull(abstractMessagePartInstance.getMessageInstance(), "MessageInstance shall also be unlinked from the part as removed from the list.");
    }

    /**
     * Test for the equals method with a null object.
     */
    @Test
    public void equalsNull() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        assertFalse(messageInstance.equals(null), "MessageInstance shall not be equal to null.");
    }

    /**
     * Test for the equals method with an instance of another class.
     */
    @Test
    public void equalsOtherClass() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        assertFalse(messageInstance.equals(3), "MessageInstance shall not be equal to an instance of another class.");
    }

    /**
     * Test for the equals method with twice the same object.
     */
    @Test
    public void equalsItself() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        assertTrue(messageInstance.equals(messageInstance), "MessageInstance shall be equal to itself.");
    }

    /**
     * Test for the identityEquals method with two objects having different UUID.
     */
    @Test
    public void equalsTestDifferentUUID() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        MessageInstance messageInstance2 = new MessageInstance(UUID2, SENDER, RECEIVER);

        assertFalse(messageInstance.identityEquals(messageInstance2), "MessageInstances with different ");
    }

    /**
     * Test for the equals method with two objects having different Message Keyword.
     */
    @Test
    public void equalsTestDifferentMessageKeyword() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance.setMessageKeyword("Tarte");
        MessageInstance messageInstance2 = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance2.setMessageKeyword("Pommes");

        assertFalse(messageInstance.equals(messageInstance2), "MessageInstances with different message Keyword shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different Message Keyword, one having none (null).
     */
    @Test
    public void equalsTestDifferentMessageKeywordNull() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance.setMessageKeyword("Tarte");
        MessageInstance messageInstance2 = new MessageInstance(UUID, SENDER, RECEIVER);

        assertFalse(messageInstance.equals(messageInstance2), "MessageInstances with different message Keyword shall not be equal.");
        assertFalse(messageInstance2.equals(messageInstance), "MessageInstances with different message Keyword shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different Timestamp.
     */
    @Test
    public void equalsTestDifferentTimestamp() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance.setTimestamp(TIMESTAMP);
        MessageInstance messageInstance2 = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance2.setTimestamp(new Date());

        assertFalse(messageInstance.equals(messageInstance2), "MessageInstances with different Timestamp shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different Timestamp, one having none (null).
     */
    @Test
    public void equalsTestDifferentTimestampNull() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance.setTimestamp(TIMESTAMP);
        MessageInstance messageInstance2 = new MessageInstance(UUID, SENDER, RECEIVER);

        assertFalse(messageInstance.equals(messageInstance2), "MessageInstances with different Timestamp shall not be equal.");
        assertFalse(messageInstance2.equals(messageInstance), "MessageInstances with different Timestamp shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different Sender.
     */
    @Test
    public void equalsTestDifferentSender() throws UnknownHostException {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance.setSender(new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor"));
        MessageInstance messageInstance2 = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance2.setSender(new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SYSTEM_UNDER_TEST, "Actor"));

        assertFalse(messageInstance.equals(messageInstance2), "MessageInstances with different Sender shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different Receiver.
     */
    @Test
    public void equalsTestDifferentReceiver() throws UnknownHostException {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance.setReceiver(new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor"));
        MessageInstance messageInstance2 = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance2.setReceiver(new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SYSTEM_UNDER_TEST, "Actor"));

        assertFalse(messageInstance.equals(messageInstance2), "MessageInstances with different Receiver shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different Sender, one having none (null).
     */
    @Test
    public void equalsTestDifferentReceiverNull() throws UnknownHostException {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        messageInstance.setReceiver(new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor"));
        MessageInstance messageInstance2 = new MessageInstance(UUID, SENDER, RECEIVER);

        assertFalse(messageInstance.equals(messageInstance2), "MessageInstances with different Receiver shall not be equal.");
        assertFalse(messageInstance2.equals(messageInstance), "MessageInstances with different Receiver shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different Metadata Instances.
     */
    @Test
    public void equalsTestMetadataInstances(){
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);
        MessageInstance messageInstance2 = new MessageInstance(UUID, SENDER, RECEIVER);

        messageInstance.addMessageMetadataInstance(new MessageMetadataInstance("Test", "Test"));

        assertFalse(messageInstance.equals(messageInstance2), "MessageInstances with different metadata shall not be equal.");
    }

    /**
     * Test for the hashCode method.
     */
    @Test
    public void hashCodeTest() {
        MessageInstance messageInstance = new MessageInstance(UUID, SENDER, RECEIVER);

        assertNotNull(messageInstance.hashCode(), "Hash code shall not be null.");
    }
}
