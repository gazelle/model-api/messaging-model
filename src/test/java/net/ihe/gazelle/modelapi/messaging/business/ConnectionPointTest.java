package net.ihe.gazelle.modelapi.messaging.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link ConnectionPoint}
 */
public class ConnectionPointTest {

    /**
     * Test for the ActorKeyword property Getter.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    @Covers(requirements = {"SIMU-069"})
    public void getActorKeywordTest() throws UnknownHostException {
        String actor = "Actor";
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR,
                actor);

        assertEquals(actor, connectionPoint.getActorKeyword(), "Returned actor keyword must be the one set in the ConnectionPoint instance.");
    }

    /**
     * Test for the ActorKeyword property Setter.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    @Covers(requirements = {"SIMU-069"})
    public void setActorKeywordTest() throws UnknownHostException {
        String actor = "Actor";
        String actor2 = "Actor2";
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR,
                actor);

        connectionPoint.setActorKeyword(actor2);

        assertEquals(actor2, connectionPoint.getActorKeyword(), "Returned actor keyword must be the one set in the ConnectionPoint instance.");
    }

    /**
     * Test for the Address property Getter.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    @Covers(requirements = {"SIMU-071"})
    public void getAddressTest() throws UnknownHostException {
        byte[] ipAddress = new byte[]{127, 0, 0, 1};
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByAddress(ipAddress), PeerType.SIMULATOR);

        assertNotNull(connectionPoint.getAddress(), "Returned address shall not be null.");
        assertTrue(Arrays.equals(ipAddress, connectionPoint.getAddress().getAddress()), "Returned InetAddress shall have the same IP address.");
    }

    /**
     * Test for the Address property Setter with a valid value (not null).
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    @Covers(requirements = {"SIMU-071"})
    public void setAddressTest() throws UnknownHostException {
        byte[] ipAddress = new byte[]{127, 0, 0, 1};
        byte[] ipAddress2 = new byte[]{127, 0, 0, 2};
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByAddress(ipAddress), PeerType.SIMULATOR);

        connectionPoint.setAddress(InetAddress.getByAddress(ipAddress2));

        assertNotNull(connectionPoint.getAddress(), "Returned address shall not be null.");
        assertTrue(Arrays.equals(ipAddress2, connectionPoint.getAddress().getAddress()), "Returned InetAddress shall have the same IP address.");
    }

    /**
     * Test for the Address property Setter for a null value.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    @Covers(requirements = {"SIMU-071"})
    public void setAddressTestNull() throws UnknownHostException {
        byte[] ipAddress = new byte[]{127, 0, 0, 1};
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByAddress(ipAddress), PeerType.SIMULATOR);

        assertThrows(IllegalArgumentException.class, () -> connectionPoint.setAddress(null), "ConnectionPoint address shall not be able to be " +
                "set to null.");
    }

    /**
     * Test for the PeerType property Getter.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    @Covers(requirements = {"SIMU-070"})
    public void getPeerTypeTest() throws UnknownHostException {
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR);

        assertEquals(PeerType.SIMULATOR, connectionPoint.getPeerType(), "Returned PeerType must be the one set in the ConnectionPoint instance.");
    }

    /**
     * Test for the PeerType property Setter for a valid value (not null).
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    @Covers(requirements = {"SIMU-070"})
    public void setPeerTypeTest() throws UnknownHostException {
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR);

        connectionPoint.setPeerType(PeerType.SYSTEM_UNDER_TEST);

        assertEquals(PeerType.SYSTEM_UNDER_TEST, connectionPoint.getPeerType(), "Returned PeerType must be the one set in the ConnectionPoint " +
                "instance.");
    }

    /**
     * Test for the PeerType property Setter for a null value.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    @Covers(requirements = {"SIMU-070"})
    public void setPeerTypeTestNull() throws UnknownHostException {
        byte[] ipAddress = new byte[]{127, 0, 0, 1};
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByAddress(ipAddress), PeerType.SIMULATOR);

        assertThrows(IllegalArgumentException.class, () -> connectionPoint.setPeerType(null), "ConnectionPoint PeeType shall not be able to be " +
                "set to null.");
    }

    /**
     * Test for the equals method with a null object.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    public void equalsNull() throws UnknownHostException {
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR);

        assertFalse(connectionPoint.equals(null), "A ConnectionPoint shall not be equal to null.");
    }

    /**
     * Test for the equals method with an instance of another class.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    public void equalsOtherClass() throws UnknownHostException {
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR);

        assertFalse(connectionPoint.equals(3), "A ConnectionPoint shall not be equal to an instance of another class.");
    }

    /**
     * Test for the equals method with twice the same object.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    public void equalsItself() throws UnknownHostException {
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR);

        assertTrue(connectionPoint.equals(connectionPoint), "A connection point shall be equal to itself.");
    }

    /**
     * Test for the equals method with two objects having different actor keywords.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    public void equalsTestDifferentActorKeyword() throws UnknownHostException {
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor");
        ConnectionPoint connectionPoint2 = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor2");

        assertFalse(connectionPoint.equals(connectionPoint2), "Two ConnectionPoints with different ActorKeyword shall not be equal.");
    }

    /**
     * Test for the equals method with two objects having different actor keywords, one having null.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    public void equalsTestDifferentActorKeywordNull() throws UnknownHostException {
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR);
        ConnectionPoint connectionPoint2 = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor2");

        assertFalse(connectionPoint.equals(connectionPoint2), "Two ConnectionPoints with different ActorKeyword shall not be equal.");
        assertFalse(connectionPoint2.equals(connectionPoint), "Two ConnectionPoints with different ActorKeyword shall not be equal.");
    }

    /**
     * Test for the equals method with two object having different Addresses.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    public void equalsTestDifferentAddress() throws UnknownHostException {
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor");
        ConnectionPoint connectionPoint2 = new ConnectionPoint(InetAddress.getByName("127.0.0.2"), PeerType.SIMULATOR, "Actor");

        assertFalse(connectionPoint.equals(connectionPoint2), "Two ConnectionPoints with different Addresses shall not be equal.");
    }

    /**
     * Test for the equals method with two object having different PeerType.
     *
     * @throws UnknownHostException if an {@link InetAddress} cannot be created for the {@link ConnectionPoint}
     */
    @Test
    public void equalsTestDifferentPeerType() throws UnknownHostException {
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR, "Actor");
        ConnectionPoint connectionPoint2 = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SYSTEM_UNDER_TEST, "Actor");

        assertFalse(connectionPoint.equals(connectionPoint2), "Two ConnectionPoints with different PeerType shall not be equal.");
    }

    /**
     * Test for the hashCode method.
     *
     * @throws UnknownHostException
     */
    @Test
    public void hashCodeTest() throws UnknownHostException {
        ConnectionPoint connectionPoint = new ConnectionPoint(InetAddress.getByName("127.0.0.1"), PeerType.SIMULATOR);

        assertNotNull(connectionPoint.hashCode(), "Hash Code for ConnectionPoint shall not be null.");
    }
}
