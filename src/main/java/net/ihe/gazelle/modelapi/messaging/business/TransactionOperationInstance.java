package net.ihe.gazelle.modelapi.messaging.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A transaction-operation-instance is an instance of an transaction operation in the IHE interoperability model (domain, integration profile, actor,
 * options and transactions).
 */
public class TransactionOperationInstance {

    private String uuid;
    private Date openingTimestamp;
    private Date closingTimestamp;
    private List<String> standardKeywords = new ArrayList<>();
    private String domainKeyword;
    private String transactionKeyword;
    private String operationKeyword;
    private ConnectionPoint initiator;
    private ConnectionPoint responder;
    private List<MessageInstance> messageInstances = new ArrayList<>();

    /**
     * Empty constructor.
     */
    private TransactionOperationInstance() {
        //Unused empty constructor.
    }

    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid             value of the entity UUID.
     * @param openingTimestamp opening timestamp for the Transaction Operation.
     * @param standardKeywords list of Standard keywords the Transaction Operation relates to.
     * @param initiator        value of the Initiator ConnectionPoint
     * @param responder        value of the responder ConnectionPoint
     * @throws IllegalArgumentException if one of the mandatory elements is null.
     */
    public TransactionOperationInstance(String uuid, Date openingTimestamp, List<String> standardKeywords, ConnectionPoint initiator,
                                        ConnectionPoint responder) {
        setUuid(uuid);
        setOpeningTimestamp(openingTimestamp);
        setStandardKeywords(standardKeywords);
        setInitiator(initiator);
        setResponder(responder);
    }

    /**
     * Getter for the uuid property.
     *
     * @return the value of the property.
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter for the uuid property
     *
     * @param uuid value to set to the property.
     * @throws IllegalArgumentException if uuid is null
     */
    public void setUuid(String uuid) {
        if (uuid != null) {
            this.uuid = uuid;
        } else {
            throw new IllegalArgumentException("UUID shall not be null.");
        }
    }

    /**
     * Getter for the uuid property.
     *
     * @return the value of the property.
     */
    public Date getOpeningTimestamp() {
        return new Date(openingTimestamp.getTime());
    }

    /**
     * Setter for the openingTimestamp property. Will create a new date based on input.
     *
     * @param openingTimestamp value to set to the property.
     * @throws IllegalArgumentException if openingTimestamp is null.
     */
    public void setOpeningTimestamp(Date openingTimestamp) {
        if (openingTimestamp != null) {
            this.openingTimestamp = new Date(openingTimestamp.getTime());
        } else {
            throw new IllegalArgumentException("Opening Timestamp shall not be null.");
        }
    }

    /**
     * Getter for the uuid property.
     *
     * @return the value of the property.
     */
    public Date getClosingTimestamp() {
        if (closingTimestamp != null) {
            return new Date(closingTimestamp.getTime());
        } else {
            return null;
        }
    }

    /**
     * Setter for the closingTimestamp property. Will create a new date based on input.
     *
     * @param closingTimestamp value to set to the property.
     */
    public void setClosingTimestamp(Date closingTimestamp) {
        if (closingTimestamp != null) {
            this.closingTimestamp = new Date(closingTimestamp.getTime());
        } else {
            this.closingTimestamp = null;
        }
    }

    /**
     * Getter for the standardKeywords property.
     *
     * @return a copy of the value of the property.
     */
    public List<String> getStandardKeywords() {
        return new ArrayList<>(standardKeywords);
    }

    /**
     * Add a Standard keyword to the standardKeywords list if it does not already exist in it.
     *
     * @param standardKeyword keyword to add.
     */
    public void addStandardKeyword(String standardKeyword) {
        if (!standardKeywords.contains(standardKeyword)) {
            standardKeywords.add(standardKeyword);
        }
    }

    /**
     * Setter for the standardKeywords property. A null input resets the list to an empty one. Otherwise the input is cloned.
     *
     * @param standardKeywords value to set to the property.
     */
    public void setStandardKeywords(List<String> standardKeywords) {
        if (standardKeywords != null) {
            this.standardKeywords = new ArrayList<>(standardKeywords);
        } else {
            this.standardKeywords = new ArrayList<>();
        }
    }

    /**
     * Getter for the domainKeyword property.
     *
     * @return the value of the property.
     */
    public String getDomainKeyword() {
        return domainKeyword;
    }

    /**
     * Setter for the domainKeyword property.
     *
     * @param domainKeyword value to set to the property.
     */
    public void setDomainKeyword(String domainKeyword) {
        this.domainKeyword = domainKeyword;
    }

    /**
     * Getter for the transactionKeyword property.
     *
     * @return the value of the property.
     */
    public String getTransactionKeyword() {
        return transactionKeyword;
    }

    /**
     * Setter for the transactionKeyword property.
     *
     * @param transactionKeyword value to set to the property.
     */
    public void setTransactionKeyword(String transactionKeyword) {
        this.transactionKeyword = transactionKeyword;
    }

    /**
     * Getter for the operationKeyword property.
     *
     * @return the value of the property.
     */
    public String getOperationKeyword() {
        return operationKeyword;
    }

    /**
     * Setter for the operationKeyword property.
     *
     * @param operationKeyword value to set to the property.
     */
    public void setOperationKeyword(String operationKeyword) {
        this.operationKeyword = operationKeyword;
    }

    /**
     * Getter for the initiator property.
     *
     * @return the value of the property.
     */
    public ConnectionPoint getInitiator() {
        return initiator;
    }

    /**
     * Setter for the initiator property.
     *
     * @param initiator value to set to the property.
     * @throws IllegalArgumentException if initiator is null
     */
    public void setInitiator(ConnectionPoint initiator) {
        if (initiator != null) {
            this.initiator = initiator;
        } else {
            throw new IllegalArgumentException("Initiator shall not be null");
        }
    }

    /**
     * Getter for the responder property.
     *
     * @return the value of the property.
     */
    public ConnectionPoint getResponder() {
        return responder;
    }

    /**
     * Setter for the responder property.
     *
     * @param responder value to set to the property.
     * @throws IllegalArgumentException if responder is null
     */
    public void setResponder(ConnectionPoint responder) {
        if (responder != null) {
            this.responder = responder;
        } else {
            throw new IllegalArgumentException("Responder shall not be null");
        }
    }

    /**
     * Getter for the abstractMessageInstances property. Returns a copy of the list.
     *
     * @return a copy of the property value.
     */
    public List<MessageInstance> getMessageInstances() {
        return new ArrayList<>(messageInstances);
    }

    /**
     * Setter for the abstractMessageInstances property. A null input resets the list to an empty one. Otherwise the input is cloned.
     *
     * @param messageInstances value to set to the property.
     */
    public void setMessageInstances(List<MessageInstance> messageInstances) {
        if (messageInstances != null) {
            this.messageInstances = new ArrayList<>(messageInstances);
        } else {
            this.messageInstances = new ArrayList<>();
        }
    }

    /**
     * Add a Message Instance to the abstractMessageInstance list if it does not already exist in it.
     *
     * @param messageInstance Message Instance to add.
     */
    public void addAbstractMessageInstance(MessageInstance messageInstance) {
        if (!messageInstances.contains(messageInstance)) {
            this.messageInstances.add(messageInstance);
        }
    }

    /**
     * Compare identity of two objects by comparing their uuid.
     *
     * @param o object to compare
     * @return true if both entity have the same identity, false otherwise.
     */
    public boolean identityEquals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TransactionOperationInstance that = (TransactionOperationInstance) o;

        return uuid.equals(that.uuid);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionOperationInstance)) return false;
        TransactionOperationInstance that = (TransactionOperationInstance) o;
        return openingTimestamp.equals(that.openingTimestamp) &&
                Objects.equals(closingTimestamp, that.closingTimestamp) &&
                standardKeywords.equals(that.standardKeywords) &&
                Objects.equals(domainKeyword, that.domainKeyword) &&
                Objects.equals(transactionKeyword, that.transactionKeyword) &&
                Objects.equals(operationKeyword, that.operationKeyword) &&
                initiator.equals(that.initiator) &&
                responder.equals(that.responder) &&
                messageInstances.equals(that.messageInstances);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = openingTimestamp.hashCode();
        result = 31 * result + (closingTimestamp != null ? closingTimestamp.hashCode() : 0);
        result = 31 * result + standardKeywords.hashCode();
        result = 31 * result + (domainKeyword != null ? domainKeyword.hashCode() : 0);
        result = 31 * result + (transactionKeyword != null ? transactionKeyword.hashCode() : 0);
        result = 31 * result + (operationKeyword != null ? operationKeyword.hashCode() : 0);
        result = 31 * result + initiator.hashCode();
        result = 31 * result + responder.hashCode();
        result = 31 * result + messageInstances.hashCode();
        return result;
    }
}
