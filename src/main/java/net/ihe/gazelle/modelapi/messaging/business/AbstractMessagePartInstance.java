package net.ihe.gazelle.modelapi.messaging.business;

import net.ihe.gazelle.lib.annotations.Covers;

/**
 * Representation of a MessageInstance in a specific standard
 */
public abstract class AbstractMessagePartInstance {

    private String uuid;
    private String messagePartKeyword;
    private String standardKeyword;
    private MessageInstance messageInstance;

    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid            value of the entity UUID
     * @param standardKeyword keyword of the standard
     */
    public AbstractMessagePartInstance(String uuid, String standardKeyword) {
        setUuid(uuid);
        setStandardKeyword(standardKeyword);
    }

    /**
     * Setter for the uuid property.
     *
     * @param uuid value to set to the property.
     * @throws IllegalArgumentException if uuid is null.
     */
    @Covers(requirements = {"SIMU-083"})
    public void setUuid(String uuid) {
        if (uuid == null) {
            throw new IllegalArgumentException("UUID shall not be null.");
        } else {
            this.uuid = uuid;
        }
    }

    /**
     * Getter for the uuid property.
     *
     * @return value of the property.
     */
    @Covers(requirements = {"SIMU-083"})
    public String getUuid() {
        return uuid;
    }

    /**
     * Getter for the messageKeyword property.
     *
     * @return value of the property.
     */
    @Covers(requirements = {"SIMU-86"})
    public String getMessagePartKeyword() {
        return messagePartKeyword;
    }

    /**
     * Setter for the messageKeyword property.
     *
     * @param messagePartKeyword keyword of the message part
     */
    @Covers(requirements = {"SIMU-86"})
    public void setMessagePartKeyword(String messagePartKeyword) {
        this.messagePartKeyword = messagePartKeyword;
    }

    /**
     * Getter for the standardKeyword property.
     *
     * @return value of the property.
     */
    @Covers(requirements = {"SIMU-084"})
    public String getStandardKeyword() {
        return standardKeyword;
    }

    /**
     * Setter for the standardKeyword property.
     *
     * @param standardKeyword keyword of the standard
     */
    @Covers(requirements = {"SIMU-084"})
    public void setStandardKeyword(String standardKeyword) {
        this.standardKeyword = standardKeyword;
    }

    /**
     * Getter for the messageInstance property.
     *
     * @return message instance the message part instance is being part of
     */
    @Covers(requirements = {"SIMU-085"})
    public MessageInstance getMessageInstance() {
        return messageInstance;
    }

    /**
     * Setter for the messageInstance property.
     *
     * @param messageInstance message instance the message part instance is being part of
     */
    @Covers(requirements = {"SIMU-085"})
    public void setMessageInstance(MessageInstance messageInstance) {
        if (messageInstance == null) {
            if (this.messageInstance != null) {
                this.messageInstance.removeMessagePartInstance(this);
            }
        } else {
            messageInstance.addMessagePartInstance(this);
        }
        this.messageInstance = messageInstance;
    }

    /**
     * Compare identity of two objects by comparing their uuid.
     *
     * @param o object to compare
     * @return true if both entity have the same identity, false otherwise.
     */
    public boolean identityEquals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractMessagePartInstance)) {
            return false;
        }

        AbstractMessagePartInstance that = (AbstractMessagePartInstance) o;

        return uuid.equals(that.uuid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractMessagePartInstance)) {
            return false;
        }

        AbstractMessagePartInstance that = (AbstractMessagePartInstance) o;

        if (messagePartKeyword != null ? !messagePartKeyword.equals(that.messagePartKeyword) : that.messagePartKeyword != null) {
            return false;
        }
        if (!standardKeyword.equals(that.standardKeyword)) {
            return false;
        }
        return messageInstance != null ? messageInstance.equals(that.messageInstance) : that.messageInstance == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = uuid.hashCode();
        result = 31 * result + (messagePartKeyword != null ? messagePartKeyword.hashCode() : 0);
        result = 31 * result + standardKeyword.hashCode();
        result = 31 * result + (messageInstance != null ? messageInstance.hashCode() : 0);
        return result;
    }
}
