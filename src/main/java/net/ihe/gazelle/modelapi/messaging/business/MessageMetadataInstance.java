package net.ihe.gazelle.modelapi.messaging.business;

/**
 * A Message-Metadata-instance is a key-value entry for holding any metadata needed by a simulation, such as validation summary, specific
 * integration-profile testing information and so on.
 */
public class MessageMetadataInstance {

    private String name;
    private String value;

    /**
     * Empty constructor for the class.
     */
    private MessageMetadataInstance() {
        //Unused empty constructor.
    }

    /**
     * Full constructor to give all properties to the new Message Metadata Instance.
     *
     * @param name  name of the metadata
     * @param value value of the metadata
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public MessageMetadataInstance(String name, String value) {
        setName(name);
        setValue(value);
    }

    /**
     * Getter for the name property.
     *
     * @return the value of the property.
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name property.
     *
     * @param name value to set to the property.
     * @throws IllegalArgumentException if name is null.
     */
    public void setName(String name) {
        if (name != null) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("MessageMetadataInstance name shall not be null !");
        }
    }

    /**
     * Getter for the value property.
     *
     * @return the value of the property.
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter for the value property.
     *
     * @param value value to set to the property.
     * @throws IllegalArgumentException if value is null.
     */
    public void setValue(String value) {
        if (value != null) {
            this.value = value;
        } else {
            throw new IllegalArgumentException("MessageMetadataInstance name shall not be null !");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MessageMetadataInstance)) return false;
        MessageMetadataInstance that = (MessageMetadataInstance) o;
        return name.equals(that.name) &&
                value.equals(that.value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
