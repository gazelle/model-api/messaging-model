package net.ihe.gazelle.modelapi.messaging.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * An abstract-message-instance is an abstract representation of a message-instance (complete message) that can be exchanged between two systems
 * during a transaction-operation-instance.
 */
public class MessageInstance {

    private String uuid;
    private String messageKeyword;
    private Date timestamp;
    private ConnectionPoint sender;
    private ConnectionPoint receiver;
    private List<MessageMetadataInstance> messageMetadataInstances = new ArrayList<>();
    private List<AbstractMessagePartInstance> abstractMessagePartInstances = new ArrayList<>();

    /**
     * Empty constructor for the class.
     */
    private MessageInstance() {
        //Emtpy.
    }

    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid     value of the entity UUID.
     * @param sender   value of the Sender ConnectionPoint
     * @param receiver value of the receiver ConnectionPoint
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public MessageInstance(String uuid, ConnectionPoint sender, ConnectionPoint receiver) {
        setUuid(uuid);
        setSender(sender);
        setReceiver(receiver);
    }

    /**
     * Getter for the uuid property.
     *
     * @return the value of the property.
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * Setter for the uuid property.
     *
     * @param uuid value to set to the property.
     * @throws IllegalArgumentException if uuid is null.
     */
    public void setUuid(String uuid) {
        if (uuid == null) {
            throw new IllegalArgumentException("UUID shall not be null.");
        } else {
            this.uuid = uuid;
        }
    }

    /**
     * Getter for the messageKeyword property.
     *
     * @return value of the property.
     */
    public String getMessageKeyword() {
        return messageKeyword;
    }

    /**
     * Setter for the messageKeyword property.
     *
     * @param messageKeyword value to set to the property.
     */
    public void setMessageKeyword(String messageKeyword) {
        this.messageKeyword = messageKeyword;
    }

    /**
     * Getter for the timestamp property.
     *
     * @return a clone of the timestamp property value.
     */
    public Date getTimestamp() {
        if (timestamp != null) {
            return new Date(timestamp.getTime());
        } else {
            return null;
        }
    }

    /**
     * Setter for the timestamp property. Clones the input value.
     *
     * @param timestamp value to set to the property.
     */
    public void setTimestamp(Date timestamp) {
        if (timestamp != null) {
            this.timestamp = new Date(timestamp.getTime());
        } else {
            this.timestamp = null;
        }
    }

    /**
     * Getter for the Sender property.
     *
     * @return value of the property.
     */
    public ConnectionPoint getSender() {
        return sender;
    }

    /**
     * Setter for the sender property.
     *
     * @param sender value to set to the property.
     * @throws IllegalArgumentException if sender is null
     */
    public void setSender(ConnectionPoint sender) {
        if (sender != null) {
            this.sender = sender;
        } else {
            throw new IllegalArgumentException("Sender shall not be null.");
        }
    }

    /**
     * Getter for the receiver property.
     *
     * @return the value of the property.
     */
    public ConnectionPoint getReceiver() {
        return receiver;
    }

    /**
     * Setter for the receiver property.
     *
     * @param receiver value to set to the property.
     * @throws IllegalArgumentException if receiver is null.
     */
    public void setReceiver(ConnectionPoint receiver) {
        if (receiver != null) {
            this.receiver = receiver;
        } else {
            throw new IllegalArgumentException("Receiver shall not be null.");
        }
    }

    /**
     * Getter for the messageMetadataInstances property.
     *
     * @return a clone of the property value.
     */
    public List<MessageMetadataInstance> getMessageMetadataInstances() {
        return new ArrayList<>(messageMetadataInstances);
    }

    /**
     * Add a Message Metadata to the messageMetadataInstances list if it does not already exist in it.
     *
     * @param messageMetadataInstance Message Metadata to add.
     */
    public void addMessageMetadataInstance(MessageMetadataInstance messageMetadataInstance) {
        if (!this.messageMetadataInstances.contains(messageMetadataInstance)) {
            this.messageMetadataInstances.add(messageMetadataInstance);
        }
    }

    /**
     * Setter for the messageMetadataInstances property. A null input resets the list to an empty one. Otherwise the input is cloned.
     *
     * @param messageMetadataInstances value to set to the property.
     */
    public void setMessageMetadataInstances(List<MessageMetadataInstance> messageMetadataInstances) {
        if (messageMetadataInstances != null) {
            this.messageMetadataInstances = new ArrayList<>(messageMetadataInstances);
        } else {
            this.messageMetadataInstances = new ArrayList<>();
        }
    }

    /**
     * Getter for the abstractMessagePartInstances property.
     *
     * @return a clone of the property value.
     */
    public List<AbstractMessagePartInstance> getAbstractMessagePartInstances() {
        return new ArrayList<>(abstractMessagePartInstances);
    }

    /**
     * Add a Message Part Instance to the abstractMessagePartInstances list if it does not already exist in it.
     *
     * @param abstractMessagePartInstance Message Metadata to add.
     */
    public void addMessagePartInstance(AbstractMessagePartInstance abstractMessagePartInstance) {
        if (abstractMessagePartInstance != null) {
            if (!this.abstractMessagePartInstances.contains(abstractMessagePartInstance)) {
                this.abstractMessagePartInstances.add(abstractMessagePartInstance);
                abstractMessagePartInstance.setMessageInstance(this);
            }
        }
    }

    /**
     * Remove a Message Part Instance to the abstractMessagePartInstances list
     *
     * @param abstractMessagePartInstance Message Metadata to add.
     */
    public void removeMessagePartInstance(AbstractMessagePartInstance abstractMessagePartInstance) {
        if (this.abstractMessagePartInstances.contains(abstractMessagePartInstance)) {
            this.abstractMessagePartInstances.remove(abstractMessagePartInstance);
            abstractMessagePartInstance.setMessageInstance(null);
        }
    }

    /**
     * Setter for the messageMetadataInstances property. A null input resets the list to an empty one. Otherwise the input is cloned.
     *
     * @param abstractMessagePartInstances value to set to the property.
     */
    public void setAbstractMessagePartInstances(List<AbstractMessagePartInstance> abstractMessagePartInstances) {
        if (abstractMessagePartInstances != null) {
            this.abstractMessagePartInstances = new ArrayList<>(abstractMessagePartInstances);
            for (AbstractMessagePartInstance abstractMessagePartInstance : abstractMessagePartInstances) {
                abstractMessagePartInstance.setMessageInstance(this);
            }
        } else {
            if (this.abstractMessagePartInstances != null) {
                for (AbstractMessagePartInstance abstractMessagePartInstance : this.abstractMessagePartInstances) {
                    abstractMessagePartInstance.setMessageInstance(null);
                }
            }
            this.abstractMessagePartInstances = new ArrayList<>();
        }
    }

    /**
     * Compare identity of two objects by comparing their uuid.
     *
     * @param o object to compare
     * @return true if both entity have the same identity, false otherwise.
     */
    public boolean identityEquals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MessageInstance that = (MessageInstance) o;

        return uuid.equals(that.uuid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MessageInstance)) return false;
        MessageInstance that = (MessageInstance) o;
        return Objects.equals(messageKeyword, that.messageKeyword) &&
                Objects.equals(timestamp, that.timestamp) &&
                sender.equals(that.sender) &&
                receiver.equals(that.receiver) &&
                messageMetadataInstances.equals(that.messageMetadataInstances) &&
                abstractMessagePartInstances.equals(that.abstractMessagePartInstances);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = messageKeyword != null ? messageKeyword.hashCode() : 0;
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        result = 31 * result + sender.hashCode();
        result = 31 * result + receiver.hashCode();
        result = 31 * result + messageMetadataInstances.hashCode();
        result = 31 * result + abstractMessagePartInstances.hashCode();
        return result;
    }
}
