package net.ihe.gazelle.modelapi.messaging.business;

/**
 * Peer type of a Connection Point.
 */
public enum PeerType {
    SIMULATOR,
    SYSTEM_UNDER_TEST,
    UNKNOWN
}
