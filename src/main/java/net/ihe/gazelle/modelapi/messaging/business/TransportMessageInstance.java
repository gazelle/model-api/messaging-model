package net.ihe.gazelle.modelapi.messaging.business;

/**
 * AbstractMessagePartInstance that it is used by an OutsideGate
 */
public abstract class TransportMessageInstance extends AbstractMessagePartInstance {
    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid            value of the entity UUID.
     * @param standardKeyword keyword of the standard of the MessageInstance.
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public TransportMessageInstance(String uuid, String standardKeyword) {
        super(uuid, standardKeyword);
    }
}
