package net.ihe.gazelle.modelapi.messaging.business;

import java.net.InetAddress;
import java.util.Objects;

/**
 * A Connection point to an actor.
 */
public class ConnectionPoint {

    private String actorKeyword;
    private InetAddress address;
    private PeerType peerType;

    /**
     * Empty constructor for the class.
     */
    private ConnectionPoint() {
        //Unused empty constructor.
    }

    /**
     * Constructor with all mandatory parameters for the class.
     *
     * @param address  address of the connection point.
     * @param peerType {@link PeerType} of the connection point.
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public ConnectionPoint(InetAddress address, PeerType peerType) {
        setAddress(address);
        setPeerType(peerType);
    }

    /**
     * Full constructor to give all properties to the new connection point.
     *
     * @param address      address of the connection point.
     * @param peerType     {@link PeerType} of the connection point
     * @param actorKeyword keyword of the Actor played by the system at this connection point.
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public ConnectionPoint(InetAddress address, PeerType peerType, String actorKeyword) {
        setAddress(address);
        setPeerType(peerType);
        setActorKeyword(actorKeyword);
    }

    /**
     * Getter for the actorKeyword property.
     *
     * @return the value of the property.
     */
    public String getActorKeyword() {
        return actorKeyword;
    }

    /**
     * Setter for the actorKeyword property.
     *
     * @param actorKeyword value to set to the property.
     */
    public void setActorKeyword(String actorKeyword) {
        this.actorKeyword = actorKeyword;
    }

    /**
     * Getter for the address property.
     *
     * @return value of the property.
     */
    public InetAddress getAddress() {
        return address;
    }

    /**
     * Setter for the address property.
     *
     * @param address value to set to the property.
     * @throws IllegalArgumentException if address is null.
     */
    public void setAddress(InetAddress address) {
        if (address != null) {
            this.address = address;
        } else {
            throw new IllegalArgumentException("Address of a connection point shall not be null !");
        }
    }

    /**
     * Getter for the peerType property.
     *
     * @return the value of the property.
     */
    public PeerType getPeerType() {
        return peerType;
    }

    /**
     * Setter for the peerType property.
     *
     * @param peerType value to set to the property.
     * @throws IllegalArgumentException if peerType is null.
     */
    public void setPeerType(PeerType peerType) {
        if (peerType != null) {
            this.peerType = peerType;
        } else {
            throw new IllegalArgumentException("PeerType of a connection point shall not be null !");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConnectionPoint)) return false;
        ConnectionPoint that = (ConnectionPoint) o;
        return Objects.equals(actorKeyword, that.actorKeyword) &&
                address.equals(that.address) &&
                peerType == that.peerType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = actorKeyword != null ? actorKeyword.hashCode() : 0;
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (peerType != null ? peerType.hashCode() : 0);
        return result;
    }
}
